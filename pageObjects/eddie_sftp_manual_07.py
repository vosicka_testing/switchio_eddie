#!/usr/bin/python
# -*- coding: utf-8 -*-
import pysftp
import os

from selenium.webdriver.common.by import By

class EddieSFTPManual07:
    #SFTP manual call
    def __init__(self, driver, network, file_type, term_id, passwd, port):
        self.driver = driver
        self.network = network
        self.file_type = file_type
        self.term_id = term_id
        self.passwd = passwd
        self.port = port

    myHostname = "193.33.23.87"
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    confirm_text = ""

    def sftp_main_function(self):

        with pysftp.Connection(host=self.myHostname, username=self.term_id, password=self.passwd, port=self.port,
                               cnopts=self.cnopts) as sftp:
            print("Network: "+self.network)
            print("Connection succesfully stablished ... ")
            # Switch to a remote directory
            sftp.cwd('/')
            # or absolute "C:\Users\sdkca\Desktop\TUTORIAL2.txt"
            # localFilePath = "../INFO21"

            localFilePath = "../networks/"+self.network+"/manual/"+self.file_type

            # Define the remote path where the file will be uploaded
            remoteFilePath = '/'+self.file_type
            sftp.put(localFilePath, remoteFilePath)
            # Obtain structure of the remote directory '/'
            directory_structure = sftp.listdir("/")

            for attr in directory_structure:
                print(attr)
                sftp.get("/" + attr, "../networks/"+self.network+"/manual/" + attr)

            f = open("../networks/"+self.network+"/manual/PRNSRV", "r")
            self.confirm_text = f.read()
            f.close()
            for attr in directory_structure:
                if os.path.exists("../networks/"+self.network+"/manual/" + attr) and attr != "INFO21" and attr != "INFO20":
                    os.remove("../networks/"+self.network+"/manual/" + attr)
            return self.confirm_text
