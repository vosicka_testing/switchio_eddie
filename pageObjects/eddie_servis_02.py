#!/usr/bin/python
# -*- coding: utf-8 -*-


from selenium.webdriver.common.by import By


class EddieServis02:
    # Role Servis má právo vidět pouze položky Terminals, Resources a TTK
    def __init__(self, driver):
        self.driver = driver

    username = (By.ID, "username")
    password = (By.ID, "password")
    log_form = (By.CSS_SELECTOR, "#loginform input[type='submit']")
    terminals_administrator = (By.XPATH, "//ul//li//span[contains(text(),'Terminals')]//parent::a/following-sibling::ul//a")
    cluster_administrator = (By.XPATH, "//ul//li//span[contains(text(),'Cluster')]//parent::a/following-sibling::ul//a")
    resources_administrator = (By.XPATH, "//ul//li//span[contains(text(),'Resources')]//parent::a/following-sibling::ul//a")
    administrator_administrator = (By.XPATH, "//ul//li//span[contains(text(),'Administrator')]//parent::a/following-sibling::ul//a")
    ttk_administrator = (By.XPATH, "//ul//li//span[contains(text(),'TTK')]//parent::a/following-sibling::ul//a")

    def fill_username(self):
        return self.driver.find_element(*EddieServis02.username)

    def fill_password(self):
        return self.driver.find_element(*EddieServis02.password)

    def confirm_form(self):
        return self.driver.find_element(*EddieServis02.log_form)

    def get_terminals(self):
        return self.driver.find_elements(*EddieServis02.terminals_administrator)

    def get_cluster(self):
        return self.driver.find_elements(*EddieServis02.cluster_administrator)

    def get_resources(self):
        return self.driver.find_elements(*EddieServis02.resources_administrator)

    def get_administrator(self):
        return self.driver.find_elements(*EddieServis02.administrator_administrator)

    def get_ttk(self):
        return self.driver.find_elements(*EddieServis02.ttk_administrator)
