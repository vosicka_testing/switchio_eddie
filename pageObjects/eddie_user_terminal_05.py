#!/usr/bin/python
# -*- coding: utf-8 -*-


from selenium.webdriver.common.by import By


class EddieUserTerminal05:
    #Role User terminal má právo vidět pouze položky Terminals a TTK
    def __init__(self, driver):
        self.driver = driver

    username = (By.ID, "username")
    password = (By.ID, "password")
    log_form = (By.CSS_SELECTOR, "#loginform input[type='submit']")
    terminals_administrator = (By.XPATH, "//ul//li//span[contains(text(),'Terminals')]//parent::a/following-sibling::ul//a")
    cluster_administrator = (By.XPATH, "//ul//li//span[contains(text(),'Cluster')]//parent::a/following-sibling::ul//a")
    resources_administrator = (By.XPATH, "//ul//li//span[contains(text(),'Resources')]//parent::a/following-sibling::ul//a")
    administrator_administrator = (By.XPATH, "//ul//li//span[contains(text(),'Administrator')]//parent::a/following-sibling::ul//a")
    ttk_administrator = (By.XPATH, "//ul//li//span[contains(text(),'TTK')]//parent::a/following-sibling::ul//a")

    def fill_username(self):
        return self.driver.find_element(*EddieUserTerminal05.username)

    def fill_password(self):
        return self.driver.find_element(*EddieUserTerminal05.password)

    def confirm_form(self):
        return self.driver.find_element(*EddieUserTerminal05.log_form)

    def get_terminals(self):
        return self.driver.find_elements(*EddieUserTerminal05.terminals_administrator)

    def get_cluster(self):
        return self.driver.find_elements(*EddieUserTerminal05.cluster_administrator)

    def get_resources(self):
        return self.driver.find_elements(*EddieUserTerminal05.resources_administrator)

    def get_administrator(self):
        return self.driver.find_elements(*EddieUserTerminal05.administrator_administrator)

    def get_ttk(self):
        return self.driver.find_elements(*EddieUserTerminal05.ttk_administrator)
