#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.eddie_sftp_manual_07 import EddieSFTPManual07
from TestData.Eddie_test_data import HomePageData
import time
import pytest


class Test07(BaseClass):
    def test_sftp_automatic(self, getData):
        tc07 = EddieSFTPManual07(self.driver, getData["network"], getData["type"], getData["term_id"], getData["passwd"], getData["port"])
        # logger
        log = self.getLogger()
        value = tc07.sftp_main_function()

        try:
            assert value.replace(".", "").strip().lower() == "tms manual call was success"
            log.info("TMS MANUAL call was success")
            state = True

        except:
            log.warning("Spatna odpoved v PRNSRV - Network: " + getData["network"] + "ID: " + getData["term_id"])
            log.warning(value)
            state = False
        assert state

    @pytest.fixture(params=HomePageData.test_07_sftp_manual)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param