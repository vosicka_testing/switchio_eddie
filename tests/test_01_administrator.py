#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.eddie_administrator_01 import EddieAdministrator01
from TestData.Eddie_test_data import HomePageData
import time
import pytest

class Test01(BaseClass):
    def test_login_eddie(self, getData):
        # definování prvního testu: Standardní platba s možností uložit kartu
        tc01 = EddieAdministrator01(self.driver)
        # logger
        log = self.getLogger()
        tc01.fill_username().send_keys(getData["login"])
        tc01.fill_password().send_keys(getData["password"])
        tc01.confirm_form().click()
        self.explicit_wait_function_ID("grafanaGraphs")

        #Get data from WEB
        terminals_administrator = tc01.get_terminals()
        cluster_administrator = tc01.get_cluster()
        resources_administrator = tc01.get_resources()
        administrator_administrator = tc01.get_administrator()
        ttk_administrator = tc01.get_ttk()

        #Original data
        terminal_items = ['merchants', 'orgunits', 'terminals', 'tags', 'termhwtypes', 'platforms', 'configurations',
                          'terminfos', 'batchupdates']
        cluster_items = ['clusterinfo', 'clusterhealth', 'clusterfservers', 'clusternotifications', 'clustergapas']
        resources_items = ["resources({resourceType: 'all', resourceInfo: null})", "resources({resourceType: 'profile', resourceInfo: 'TERMINAL'})",
                           "resources({resourceType: 'profile', resourceInfo: 'APPLICATION'})", "resources({resourceType: 'binary', resourceInfo: null})",
                           "resources({resourceType: 'parameter', resourceInfo: null})", "resources({resourceType: 'brand', resourceInfo: null})",
                           "resources({resourceType: 'key', resourceInfo: null})", "resources({resourceType: 'file', resourceInfo: null})"]
        administrator_items = ['users','groups','roles','networkGroups','networks','issuerMapping','rparametertypes',
                               'feservers','sftpconfigs','ftpsconfigs','imports','processorConfig','gapaservers',
                               'notifyConfig','journalNotify','grafanas']
        ttk_items = ['cards', 'ttkgens']

        try:
            for ta in terminals_administrator:
                assert ta.get_attribute("ui-sref") in terminal_items
            log.info("Sekce Terminals OK")

            for ca in cluster_administrator:
                assert ca.get_attribute("ui-sref") in cluster_items
            log.info("Sekce Cluster OK")

            for ra in resources_administrator:
                assert ra.get_attribute("ui-sref") in resources_items
            log.info("Sekce Resources OK")

            for aa in administrator_administrator:
                assert aa.get_attribute("ui-sref") in administrator_items
            log.info("Sekce Administrator OK")

            for tta in ttk_administrator:
                assert tta.get_attribute("ui-sref") in ttk_items
            log.info("Sekce TTK OK")
            state = True
            
        except:
            log.error("Administrator nevidi spravne prvky v menu")
            state = False

        assert state

    @pytest.fixture(params=HomePageData.test_01_administrator)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param