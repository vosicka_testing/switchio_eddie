#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.eddie_servis_02 import EddieServis02
from TestData.Eddie_test_data import HomePageData
import time
import pytest


class Test02(BaseClass):
    def test_login_eddie(self, getData):
        # definování prvního testu: Standardní platba s možností uložit kartu
        tc02 = EddieServis02(self.driver)
        # logger
        log = self.getLogger()
        tc02.fill_username().send_keys(getData["login"])
        tc02.fill_password().send_keys(getData["password"])
        tc02.confirm_form().click()
        self.explicit_wait_function_ID("grafanaGraphs")

        # Get data from WEB
        terminals_administrator = tc02.get_terminals()
        cluster_administrator = tc02.get_cluster()
        resources_administrator = tc02.get_resources()
        administrator_administrator = tc02.get_administrator()
        ttk_administrator = tc02.get_ttk()

        # Original data
        terminal_items = ['merchants', 'orgunits', 'terminals', 'tags', 'termhwtypes', 'platforms', 'configurations',
                          'terminfos', 'batchupdates']
        resources_items = ["resources({resourceType: 'all', resourceInfo: null})",
                           "resources({resourceType: 'profile', resourceInfo: 'TERMINAL'})",
                           "resources({resourceType: 'profile', resourceInfo: 'APPLICATION'})",
                           "resources({resourceType: 'binary', resourceInfo: null})",
                           "resources({resourceType: 'parameter', resourceInfo: null})",
                           "resources({resourceType: 'brand', resourceInfo: null})",
                           "resources({resourceType: 'key', resourceInfo: null})",
                           "resources({resourceType: 'file', resourceInfo: null})"]
        ttk_items = ['cards', 'ttkgens']

        try:
            assert self.get_lenght_val(terminals_administrator) > 0
            for ta in terminals_administrator:
                assert ta.get_attribute("ui-sref") in terminal_items
            log.info("Sekce Terminals OK")

            assert len(cluster_administrator) == 0
            log.info("Sekce Cluster OK nema pristup")

            assert self.get_lenght_val(resources_administrator) > 0
            for ra in resources_administrator:
                assert ra.get_attribute("ui-sref") in resources_items
            log.info("Sekce Resources OK")

            assert len(administrator_administrator) == 0
            log.info("Sekce Administrator OK nema pristup")

            assert self.get_lenght_val(ttk_administrator) > 0
            for tta in ttk_administrator:
                assert tta.get_attribute("ui-sref") in ttk_items
            log.info("Sekce TTK OK")
            state = True

        except:
            log.error("Role SERVIS nevidi spravne prvky v menu")
            state = False

        assert state

    @pytest.fixture(params=HomePageData.test_02_servis)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param