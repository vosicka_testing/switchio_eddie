#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.eddie_user_terminal_05 import EddieUserTerminal05
from TestData.Eddie_test_data import HomePageData
import time
import pytest


class Test05(BaseClass):
    def test_login_eddie(self, getData):
        tc05 = EddieUserTerminal05(self.driver)
        # logger
        log = self.getLogger()
        tc05.fill_username().send_keys(getData["login"])
        tc05.fill_password().send_keys(getData["password"])
        tc05.confirm_form().click()
        self.explicit_wait_function_ID("grafanaGraphs")

        # Get data from WEB
        terminals_administrator = tc05.get_terminals()
        cluster_administrator = tc05.get_cluster()
        resources_administrator = tc05.get_resources()
        administrator_administrator = tc05.get_administrator()
        ttk_administrator = tc05.get_ttk()

        # Original data
        terminal_items = ['merchants', 'orgunits', 'terminals', 'tags', 'termhwtypes', 'platforms', 'configurations',
                          'terminfos', 'batchupdates']
        ttk_items = ['cards', 'ttkgens']

        try:
            assert self.get_lenght_val(terminals_administrator) > 0
            for ta in terminals_administrator:
                assert ta.get_attribute("ui-sref") in terminal_items
            log.info("Sekce Terminals OK")

            assert len(cluster_administrator) == 0
            log.info("Sekce Cluster OK nema pristup")

            assert len(resources_administrator) == 0
            log.info("Sekce Resources OK nema pristup")

            assert len(administrator_administrator) == 0
            log.info("Sekce Administrator OK nema pristup")

            assert self.get_lenght_val(ttk_administrator) > 0
            for tta in ttk_administrator:
                assert tta.get_attribute("ui-sref") in ttk_items
            log.info("Sekce TTK OK")
            state = True

        except:
            log.error("Administrator nevidi spravne prvky v menu")
            state = False

        assert state

    @pytest.fixture(params=HomePageData.test_05_user_terminal)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param