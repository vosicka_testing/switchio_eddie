#!/usr/bin/python
# -*- coding: utf-8 -*-

import pytest
from selenium import webdriver
import time



driver = None

def pytest_addoption(parser):
    parser.addoption(
        "--browser_name", action="store", default="chrome"
    )
    parser.addoption(
        "--environment", action="store", default="model"
    )

@pytest.fixture(scope="class")
def setup(request):
    global driver
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_argument("headless")

    value = request.config.getoption("--browser_name")
    environment = request.config.getoption("--environment")
    env_path = ""
    login_eddie = ""
    password_eddie = ""

    if value == "firefox":
        driver = webdriver.Firefox(
            executable_path=r'C:\Users\vosicka\Documents\switchio_eddie\drivers\geckodriver.exe')
    elif value == "chrome":
        #driver = webdriver.Chrome(executable_path=r'C:\Users\vosicka\Documents\switchio_eddie\drivers\chromedriver.exe')
        driver = webdriver.Chrome(executable_path=r'C:\Users\vosicka\Documents\switchio_eddie\drivers\chromedriver.exe')
            #executable_path='/home/osboxes/Desktop/platebni_brana/monet_platebni_brana/drivers/chromedriver')
            #executable_path='./drivers/chromedriver')
    elif value == "edge":
        driver = webdriver.Edge(
            executable_path=r'C:\Users\vosicka\Documents\MEGAsync\Tests\PythonSelFramework\drivers\msedgedriver.exe')
    elif value == "opera":
        driver = webdriver.Opera(
            executable_path=r'C:\Users\vosicka\Desktop\operadriver.exe')
    if environment == "model":
        driver.implicitly_wait(5)
        env_path = "https://vpn-meddie-spc.monetplus.cz/"
        driver.get("https://vpn-meddie-spc.monetplus.cz/")
    elif environment == "prod":
        driver.implicitly_wait(5)
        env_path = "https://vpn-meddie-spc.monetplus.cz/"
        driver.get("https://vpn-meddie-spc.monetplus.cz/")

    driver.maximize_window()
    request.cls.login_eddie = login_eddie
    request.cls.password_eddie = password_eddie
    request.cls.env_path = env_path
    request.cls.driver = driver
    yield
    time.sleep(3)
    driver.close()



@pytest.mark.hookwrapper
def pytest_runtest_makereport(item):
    """
        Extends the PyTest Plugin to take and embed screenshot in html report, whenever test fails.
        :param item:
        """
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])

    if report.when == 'call' or report.when == "setup":
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            file_name = report.nodeid.replace("::", "_") + ".png"
            _capture_screenshot(file_name)
            if file_name:
                html = '<div><img src="%s" alt="screenshot" style="width:304px;height:228px;" ' \
                       'onclick="window.open(this.src)" align="right"/></div>' % file_name
                extra.append(pytest_html.extras.html(html))
        report.extra = extra


def _capture_screenshot(name):
        driver.get_screenshot_as_file(name)

