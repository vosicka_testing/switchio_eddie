#!/usr/bin/python
# -*- coding: utf-8 -*-

class HomePageData:

    test_01_administrator = [
        {"login":"vosicka","password":"Sxapk181998@"}]

    test_02_servis = [
        {"login":"auttestyservis","password":"Sxapk181996@"}]

    test_03_servis_terminal = [
        {"login": "auttestyservisterm", "password": "Sxapk181996@"}]

    test_04_user = [
        {"login": "auttestyuser", "password": "Sxapk181996@"}]

    test_05_user_terminal = [
        {"login": "auttesty", "password": "Sxapk181995"}]

    test_06_sftp_automatic = [
        {"network": "TMONET", "type": "INFO21", "term_id": "MOTEST01", "passwd": "EDDIE", "port": 7820},
        {"network": "TSB", "type": "INFO21", "term_id": "SBEPNGT001", "passwd": "M0NET2SB", "port": 7320},
        #{"network": "TFISKALPRO", "type": "INFO20","term_id": "SBEPNGT001", "passwd": "M0NET2SB", "port": 7920},
        {"network": "TPLUSPRO", "type": "INFO21", "term_id": "PPROSA03", "passwd": "EDDIE", "port": 7220}
    ]
    test_07_sftp_manual = [
        {"network": "TMONET", "type": "INFO21", "term_id": "MOTEST01", "passwd": "EDDIE", "port": 7820},
        {"network": "TSB", "type": "INFO21", "term_id": "SBEPNGT001", "passwd": "M0NET2SB", "port": 7320},
        {"network": "TFISKALPRO", "type": "INFO20","term_id": "A3SK0001", "passwd": "heslo123", "port": 7920},
        {"network": "TPLUSPRO", "type": "INFO21", "term_id": "PPROSA03", "passwd": "EDDIE", "port": 7220}
    ]